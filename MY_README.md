#README.md

##Purpose
Purpose: to provide witty excuses on how you are "challenged"

example:


	###**diabolically** challenged

	the challenges faced by the devil

	Example:

	The devil's not evil, he's just diabolically challenged.


Features:
* leaderboard
* create/delete/update/
* login


##Model Relationships

user has_many definition (that he created)
user belongs_to_many definition (that he liked)
definition belongs_to_many user (that liked him)
definition belongs_to user
definition belongs_to adverb
adverb has_many definition
