<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	#+------------------
	#| Relationships
	#+------------------
	public function total_points(){ #User + Guest
		return ($this->user_points())*5 + $this->guest_points();
	}

	public function guest_points(){
		//Realistically, I should be using accessors, but this works.
		$definitions = Definition::where('user_id', '=', $this->id)->get();
		$points = 0;
		$definitions = $definitions->toArray(); #cast to array so we can cast stuff to ints
		foreach($definitions as $definition => $data){
			$points += $data['guest_points'];
		};
		return $points;
	}

	public function user_points()
	{
		return $this->belongsToMany('Definition')->count();
	}

	public function definitions()
	{
		$definitions = Definition::where('user_id', '=', $this->id)->get();
		$definitions->sortBy(function($def)
		{
			return $def->adverb()->adverb;
		});

		return $definitions;
	}

	public function rank()
	{
		$ladder = $this->all();

		$ladder = $ladder->sortBy(function($user)
		{
			return $user->total_points();
		})->reverse()->toArray();

		$rank = 0;
		foreach ($ladder as $item){
			if ($item['id'] == Auth::user()->id){
				$rank += 1;
				break;
			}
			else{
				$rank += 1;
			}
		}

		return $rank;
	}

	#+------------------
	#| Remember feature
	#+------------------ 
	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

}
