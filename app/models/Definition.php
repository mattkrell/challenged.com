<?php

class Definition extends Eloquent {
  public function user()
  {
    return $this->belongsTo('User')->first();
  }

  public function adverb()
  {
    return $this->belongsTo('Adverb')->first();
  }

  public function points()
  {
    return $this->belongsToMany('User')->count();
  }

  public function likes()
  {
    return $this->belongsToMany('User');
  }

  public function liked_user($id)
  {
    return $this->belongsToMany('User')->where('id', '=', $id)->first();
  }

  public function total_points()
  {
    return ($this->belongsToMany('User')->count())*5 + $this->guest_points; # UserPoints * 5 + GuestPoints
  }

}
