<?php

use Faker\Factory as Faker;

class AdverbsTableSeeder extends Seeder {
	public function run()
	{
	    $faker = Faker::create();

        foreach(range(1, 30) as $index)
        {
            Adverb::create([
                'adverb' => $faker->word .'ly'
            ]);
        }

	}
}