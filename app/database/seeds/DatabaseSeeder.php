<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		Adverb::truncate(); 
		User::truncate();
		Definition::truncate();
		DB::table('definition_user')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Eloquent::unguard();

		
		$this->call('AdverbsTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('DefinitionsTableSeeder');
		$this->call('DefinitionUserTableSeeder');
	}

}