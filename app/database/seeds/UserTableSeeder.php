<?php

use Faker\Factory as Faker;

class UserTableSeeder extends Seeder {
	public function run()
	{
	    $faker = Faker::create();

        foreach(range(1, 30) as $index)
        {
            User::create([
                'username'  => $faker->userName,
                'password'  => Hash::make('password'),
                'email'     => $faker->email
            ]);
        }

	}
}