<?php

use Faker\Factory as Faker;

class DefinitionsTableSeeder extends Seeder {
	public function run()
	{
	    $faker = Faker::create();
        $userIds = User::lists('id');
        $adverbIds = Adverb::lists('id');

        foreach(range(1, 30) as $index)
        {
            Definition::create([
                'adverb_id'     => $faker->randomElement($adverbIds),
                'body'          => $faker->paragraph(3),
                'example'       => $faker->sentence(10),
                'user_id'       => $faker->randomElement($adverbIds),
                'guest_points'  => $faker->randomNumber(1, 30)
            ]);
        }

	}
}