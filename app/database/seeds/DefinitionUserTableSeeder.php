<?php

use Faker\Factory as Faker;

class DefinitionUserTableSeeder extends Seeder {
	public function run()
	{
	    $faker = Faker::create();
        $userIds = User::lists('id');
        $definitionIds = Definition::lists('id');

        foreach(range(1, 40) as $index)
        {
            DB::table('definition_user')->insert([
                'definition_id' => $faker->randomElement($definitionIds),
                'user_id'       => $faker->randomElement($userIds)
            ]);
        }

	}
}