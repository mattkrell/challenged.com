<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
|-------------------------
| Homepage | Leaderboards
|-------------------------
*/

Route::get('/', array('as' => 'home', function()
{

	// Grab all the entries for UserPoints
	$entries = Definition::all();

	// Sort $enties by points (and reverse to desc)
	$entries = $entries->sortBy(function($definition)
	{
		return $definition->points();
	})->reverse();
	$entries = $entries->slice(0, 5);


	// Grab all the entries for GuestPoints
	$guests = Definition::all();

	// Sort $guests by points (and reverse to desc)
	$guests = $guests->sortBy(function($definition)
	{
		return $definition->guest_points;
	})->reverse();
	$guests = $guests->slice(0, 5);


	// Grab all the entries for UserPoints + GuestPoints
	$totals = Definition::all();

	// Sort $totals by points (and reverse to desc)
	$totals = $totals->sortBy(function($definition)
	{
		return $definition->total_points();
	})->reverse();
	$totals = $totals->slice(0, 5);


	// Grab all the entries for User Leaderboard
	$userboard = User::all();

	// Sort $userboard by points (and reverse to desc)
	$userboard = $userboard->sortBy(function($user)
	{
		return $user->total_points();
	})->reverse();
	$userboard = $userboard->slice(0, 5);

	// Return the view
	return View::make('home', compact('entries', 'guests', 'totals', 'userboard'));
}));


Route::get('/leaderboards', array('as' => 'leaderboards', function()
{
	// Grab all the entries for UserPoints
	$entries = Definition::all();

	// Sort $enties by points (and reverse to desc)
	$entries = $entries->sortBy(function($definition)
	{
		return $definition->points();
	})->reverse();

	// Grab all the entries for GuestPoints
	$guests = Definition::all();

	// Sort $guests by points (and reverse to desc)
	$guests = $guests->sortBy(function($definition)
	{
		return $definition->guest_points;
	})->reverse();

	// Grab all the entries for UserPoints + GuestPoints
	$totals = Definition::all();

	// Sort $totals by points (and reverse to desc)
	$totals = $totals->sortBy(function($definition)
	{
		return $definition->total_points();
	})->reverse();

	// Grab all the entries for User Leaderboard
	$userboard = User::all();

	// Sort $userboard by points (and reverse to desc)
	$userboard = $userboard->sortBy(function($user)
	{
		return $user->total_points();
	})->reverse();

	//return the view;
	return View::make('leaderboards', compact('entries', 'guests', 'totals', 'userboard'));
}));

/*
|-------------------------
| Logins
|-------------------------
*/

Route::get('/login', function()
{
	return View::make('login');
});

Route::post('/login', function()
{
	$credentials = Input::only('email', 'password');
	if (Auth::attempt($credentials)){
		return Redirect::intended('/');
	}
	Input::flash();
	return Redirect::to('login')->with('message', 'Error: Wrong username or password');
});


Route::get('/logout', function()
{
	Auth::logout();

	return Redirect::to('/');
});

/*
|-------------------------
| Signups
|-------------------------
*/

Route::get('signup', function()
{
	return View::make('signup');
});

Route::post('signup', function()
{
	$data = Input::all();

	$rules = array(
		'username' => 'alpha_num|required',
		'password' => 'required|confirmed',
		'email'		=> 'required'
	);

	# all passwords, for testing, I am inputting as 'password'

	$validator = Validator::make($data, $rules);

	if($validator->passes()){
		$user = new User;
		$user->username = Input::get('username');
		$user->password = Hash::make(Input::get('password'));
		$user->email = Input::get('email');
		$user->save();

		//Log user in
		Auth::login($user);

		return Redirect::to('/');
	}
	Input::flash();
	return Redirect::to('/signup')->withErrors($validator);
});

/*
|-------------------------
| About
|-------------------------
*/

Route::get('/about', array('as' => 'about', function()
{
	return View::make('about');
}));


/*
|-------------------------
| Browse | Create
|-------------------------
*/

Route::resource('/definitions', 'DefinitionController');

/*
|-------------------------
| Vote
|-------------------------
*/

Route::get('/vote', array('as' =>'vote', 'uses'=>'PageController@showVote'));

Route::post('/vote', array('uses' => 'PageController@handleVote'));

/*
|-------------------------
| Test route
|-------------------------
*/

Route::get('/test', function()
{
	return View::make('test');
});

/*
|-------------------------
| Profile		#this should really be in Page controller
|-------------------------
*/

Route::get('/profile', function()
{
	if(!Auth::check()){
		return Redirect::to('/')->with('message', "I'm sorry, you can't do that >:P");
	}

	$definitions = Auth::user()->definitions();

	return View::make('profile', compact('definitions'));
});

Route::post('/profile/usernamereset', function()
{
	$username = Input::get('username');
	$user = User::find(Auth::user()->id);
	$user->username = $username;
	$user->save();
	return Redirect::to('/')->with('message', 'Username changed.');
});

Route::get('/reset', array('uses' => 'RemindersController@getRemind'));

Route::post('/profile/passwordreset', array('uses' => 'RemindersController@postRemind'));

Route::get('/404', function(){
	return View::make('errors.404');
});
