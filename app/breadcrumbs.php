<?php

/*|----------------------------
  | Breadcrumbs
  |----------------------------
*/

Breadcrumbs::register('home', function($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

Breadcrumbs::register('vote', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Vote', route('vote'));
});

Breadcrumbs::register('about', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('About', route('about'));
});

Breadcrumbs::register('profile', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Profile', url('profile'));
});

Breadcrumbs::register('browse', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Definitions', url('definitions'));
});

Breadcrumbs::register('single', function($breadcrumbs, $id) {
    $breadcrumbs->parent('browse');
    $breadcrumbs->push($id, url('definitions', $id));
});

Breadcrumbs::register('edit', function($breadcrumbs, $id) {
    $breadcrumbs->parent('single', $id);
    $breadcrumbs->push('Edit', url($id));
});

Breadcrumbs::register('create', function($breadcrumbs) {
    $breadcrumbs->parent('browse');
    $breadcrumbs->push('Create', url('create'));
});

Breadcrumbs::register('leaderboards', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Leaderboards', url('leaderboards'));
});
