@extends('layout')

@section('content')

{{ Breadcrumbs::render('about') }}

<p>This website built and maintained by Matthew Krell. Contact him at</p>

<address>
  <strong>Matthew Krell</strong><br>
  mkrell@bethanyassociates.com<br>
</address>

<address>
  <strong>Bethany Associates Inc.</strong><br>
  xxx 8th Street<br>
  Hammonton, NJ<br>
  xxx-xxx-xxxx<br>
</address>

<p>This website is built on <a href="http://laravel.com">Laravel</a>, the PHP framework for web artisans.

<p>CSS and Javascript by <a href="http://getbootstrap.com">Bootstrap</a>, the most popular front-end framework for developing responsive, mobile first projects on the web.

<p><a href="http://glyphicons.com/">Glypicons.com</a> has provided its halflings icons for free with Bootstrap. They are used with pride on this site.</p>
@stop
