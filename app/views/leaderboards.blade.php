@extends('layout')

@section('content')

{{ Breadcrumbs::render('leaderboards') }}

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#topuser" data-toggle="tab">Top Users</a></li>
  <li><a href="#total" data-toggle="tab">Total Points</a></li>
  <li><a href="#user" data-toggle="tab">User Points</a></li>
  <li><a href="#guest" data-toggle="tab">Guest Points</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">

  <div class="tab-pane active" id="topuser"> <!-- TOP USERS -->

    <h3>Top Users</h3>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Rank</th>
          <th>User</th>
          <th>Total Points</th>
          <th>Guest Points</th>
          <th>User Points</th>
          <th>Definitions</th>
        </tr>
      </thead>
      <?php $rank = 1; #create rank?>
    @foreach($userboard as $user)
      <tr {{ (isset(Auth::user()->id) && Auth::user()->id == $user->id) ? "  class='info'": ''}}>
        <td>
          <div class="col-md-1">{{$rank}}</div>
        </td>
        <td>
          <strong> {{ $user->username }}{{ (isset(Auth::user()->id) && Auth::user()->id == $user->id) ? " <span class='glyphicon glyphicon-user'></span>" : ''}}</strong>
        </td>
        <td>
          <span class="badge">{{$user->total_points()}}</span>
        </td>
        <td>
          <span class="badge">{{$user->guest_points()}}</span>
        </td>
        <td>
          <span class="badge">{{$user->user_points()}}</span>
        </td>
        <td>
          <span class="badge">{{$user->definitions()->count()}}</span>
        </td>
        <?php $rank++; ?>
      </tr>

    @endforeach
    </table>

  </div>

  <div class="tab-pane" id="total"> <!-- TOP USERS -->
    <h3>Totals Leaderboard <small>(UserPoints <span class="glyphicon glyphicon-remove"></span> 5 ) <span class="glyphicon glyphicon-plus"></span> GuestPoints</small></h3>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>Rank</th>
          <th>Total Points</th>
          <th>Adverb</th>
          <th>Body</th>
          <th>Example</th>
          <th>User</th>
        </tr>
      </thead>
      <?php $rank = 1; #create rank?>
    @foreach($totals as $total)
      <tr {{ (isset(Auth::user()->id) && Auth::user()->id == $total->user()->id) ? "  class='info'": ''}}>
        <td>
          {{$rank}}
        </td>
        <td>
          <span class="badge">{{$total->total_points()}}</span>
        </td>
        <td>
          <a href='{{ url("/definitions/$total->id")}}'>{{ $total->adverb()->adverb }}</a>
        </td>
        <td>
          {{ $total->body }}
        </td>
        <td>
          {{ $total->example }}
        </td>
        <td>
          <strong>{{ $total->user()->username }}{{ (isset(Auth::user()->id) && Auth::user()->id == $total->user()->id) ? "  <span class='glyphicon glyphicon-user'></span>": ''}}</strong>
        </td>
        <?php $rank++; ?>
      </tr>

    @endforeach
    </table>
  </div>

  <div class="tab-pane" id="user"> <!-- USER POINTS -->
    <h3>UserPoints Leaderboard <small>UserPoints are points given by registered users. <a href="{{ url('login') }}">Login</a> or <a href="{{ url('signup') }}">signup</a> now!</small></h3>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Rank</th>
          <th>UserPoints</th>
          <th>Adverb</th>
          <th>Body</th>
          <th>Example</th>
          <th>User</th>
        </tr>
      </thead>
      <?php $rank = 1; #create rank?>
    @foreach($entries as $entry)
      <tr {{ (isset(Auth::user()->id) && Auth::user()->id == $entry->user()->id) ? "  class='info'": ''}}>
        <td>
          {{$rank}}
        </td>
        <td>
          <span class="badge">{{$entry->points()}}</span>
        </td>
        <td>
          <a href='{{ url("/definitions/$entry->id")}}'>{{ $entry->adverb()->adverb }}</a>
        </td>
        <td>
          {{ $entry->body }}
        </td>
        <td>
          {{ $entry->example }}
        </td>
        <td>
          <strong>{{ $entry->user()->username }}{{ (isset(Auth::user()->id) && Auth::user()->id == $entry->user()->id) ? "  <span class='glyphicon glyphicon-user'></span>": ''}}</strong>
        </td>
        <?php $rank++ ?>
      </tr>

    @endforeach
    </table>
  </div>

  <div class="tab-pane" id="guest"> <!-- GUEST POINTS -->
    <h3>GuestPoints Leaderboard <small>Anyone can give GuestPoints! They count as just 1 point.</small></h3>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Rank</th>
          <th>GuestPoints</th>
          <th>Adverb</th>
          <th>Body</th>
          <th>Example</th>
          <th>User</th>
        </tr>
      </thead>
      <?php $rank = 1; #create rank?>
    @foreach($guests as $guest)
      <tr {{ (isset(Auth::user()->id) && Auth::user()->id == $guest->user()->id) ? "  class='info'": ''}}>
        <td>
          {{$rank}}
        </td>
        <td>
          <span class="badge">{{$guest->guest_points}}</span>
        </td>
        <td>
          <a href='{{ url("/definitions/$guest->id")}}'>{{ $guest->adverb()->adverb }}</a>
        </td>
        <td>
          {{ $guest->body }}
        </td>
        <td>
          {{ $guest->example }}
        </td>
        <td>
          <strong>{{ $guest->user()->username }}{{ (isset(Auth::user()->id) && Auth::user()->id == $guest->user()->id) ? "  <span class='glyphicon glyphicon-user'></span>": ''}}</strong>
        </td>
        <?php $rank ++?>
      </tr>

    @endforeach
    </table>
  </div>
</div>

@stop
