@extends('layout')

@section('content')

@if(Session::get('message'))
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  {{ Session::get('message') }}
</div>
@endif

<p class="lead">Are you a victim? Can't do things others can do? Need a politically correct excuse for bad behavior? You just might be CHALLENGED!</p>

<p>Challenged.com is a site dedicated to all definitions like the following:</p>

<blockquote><p>The Devil's not evil. He's just <strong>diabolically</strong> challenged.</p>

<p>You don't have bad cooking, you are just <strong>culinarily</strong> challenged.</p>

<p>I'm not unpopular, I'm just <strong>politically</strong> challenged.</p>
</blockquote>

<p class="lead">What's <em>YOUR</em> excuse?</p>
<p>Wanna get ranked? Go ahead and <a href="{{url('/definitions/create')}}">make</a> a definition yourself! It's fast and free!</p>



<h2>Leaderboards</h2>

<h3>Top Users</h3>
<table class="table table-striped">
  <thead>
    <tr>
      <th>User</th>
      <th>Total Points</th>
      <th>Guest Points</th>
      <th>User Points</th>
      <th>Definitions</th>
    </tr>
  </thead>
@foreach($userboard as $user)
  <tr {{ (isset(Auth::user()->id) && Auth::user()->id == $user->id) ? "  class='info'": ''}}>
    <td>
      <strong>{{ $user->username }}{{ (isset(Auth::user()->id) && Auth::user()->id == $user->id) ? " <span class='glyphicon glyphicon-user'></span>" : ''}}</strong>
    </td>
    <td>
      <span class="badge">{{$user->total_points()}}</span>
    </td>
    <td>
      <span class="badge">{{$user->guest_points()}}</span>
    </td>
    <td>
      <span class="badge">{{$user->user_points()}}</span>
    </td>
    <td>
      <span class="badge">{{$user->definitions()->count()}}</span>
    </td>
  </tr>

@endforeach
</table>


<h3>Totals Leaderboard <small>(UserPoints <span class="glyphicon glyphicon-remove"></span> 5) <span class="glyphicon glyphicon-plus"></span> GuestPoints</h3>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Total Points</th>
      <th>Adverb</th>
      <th>Body</th>
      <th>Example</th>
      <th>User</th>
    </tr>
  </thead>

@foreach($totals as $total)
  <tr {{ (isset(Auth::user()->id) && Auth::user()->id == $total->user()->id) ? "  class='info'": ''}}>
    <td>
      <span class="badge">{{$total->total_points()}}</span>
    </td>
    <td>
      {{ $total->adverb()->adverb }}
    </td>
    <td>
      {{ $total->body }}
    </td>
    <td>
      {{ $total->example }}
    </td>
    <td>
      <strong>{{ $total->user()->username }}{{ (isset(Auth::user()->id) && Auth::user()->id == $total->user()->id) ? "  <span class='glyphicon glyphicon-user'></span>": ''}}</strong>
    </td>
  </tr>

@endforeach
</table>



<h3>UserPoints Leaderboard <small>UserPoints are points given by registered users. <a href="{{ url('login') }}">Login</a> or <a href="{{ url('signup') }}">signup</a> now!</small></h3>
<table class="table table-striped">
  <thead>
    <tr>
      <th>UserPoints</th>
      <th>Adverb</th>
      <th>Body</th>
      <th>Example</th>
      <th>User</th>
    </tr>
  </thead>
@foreach($entries as $entry)
  <tr {{ (isset(Auth::user()->id) && Auth::user()->id == $entry->user()->id) ? "  class='info'": ''}}>
    <td>
      <span class="badge">{{$entry->points()}}</span>
    </td>
    <td>
      {{ $entry->adverb()->adverb }}
    </td>
    <td>
      {{ $entry->body }}
    </td>
    <td>
      {{ $entry->example }}
    </td>
    <td>
      <strong>{{ $entry->user()->username }}{{ (isset(Auth::user()->id) && Auth::user()->id == $entry->user()->id) ? "  <span class='glyphicon glyphicon-user'></span>": ''}}</strong>
    </td>
  </tr>

@endforeach
</table>



<h3>GuestPoints Leaderboard <small>Anyone can give GuestPoints! They count as just 1 point.</small></h3>
<table class="table table-striped">
  <thead>
    <tr>
      <th>GuestPoints</th>
      <th>Adverb</th>
      <th>Body</th>
      <th>Example</th>
      <th>User</th>
    </tr>
  </thead>
@foreach($guests as $guest)
  <tr {{ (isset(Auth::user()->id) && Auth::user()->id == $guest->user()->id) ? "  class='info'": ''}}>
    <td>
      <span class="badge">{{$guest->guest_points}}</span>
    </td>
    <td>
      {{ $guest->adverb()->adverb }}
    </td>
    <td>
      {{ $guest->body }}
    </td>
    <td>
      {{ $guest->example }}
    </td>
    <td>
      <strong>{{ $guest->user()->username }}{{ (isset(Auth::user()->id) && Auth::user()->id == $guest->user()->id) ? "  <span class='glyphicon glyphicon-user'></span>": ''}}</strong>
    </td>
  </tr>

@endforeach
</table>






@stop
