@extends('layout')

@section('content')

<div class="panel panel-default">
  <div class="panel-body">
    @if (Session::get('message'))
    <span class="alert alert-warning">{{Session::get('message')}}</span>
    @else
    <strong>Please Login</strong> | <small> Don't have an account? <a href="{{ url('signup') }}" class="btn btn-default">Signup</a></small>
    @endif
  </div>
</div>


{{ Form::open() }}

  <div class="form-group">
    <h4>Email</h4>
    {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'email'))}}
  </div>
  <div class="form-group">
    <h4>Password</h4>
    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'password')) }}
  </div>
  <button type="submit" class="btn btn-default">Submit</button>


{{ Form::close() }}



@stop
