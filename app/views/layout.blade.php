<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset="UTF-8">
	<title>Challenged.com</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
</head>
<body>
	<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
	<div class="container">
    <div class="page-header"><h1>Challenged.com<small> Are you Challenged?</small></h1></div>


		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">

				<div class="navbar-header">
					<div class="nav navbar-nav">
						<a href="{{ route('home') }}" class='navbar-brand'>home</a>
						<a href="{{ route('vote') }}" class='navbar-brand'><strong>vote</strong></a>
		        <a href="{{ action('DefinitionController@index') }}" class='navbar-brand'>browse</a>
		        <a href="{{ action('DefinitionController@create')}}" class='navbar-brand'>create</a>
		        <a href="{{ route('about') }}" class='navbar-brand'>about</a>
						<a href="{{ route('leaderboards') }}" class='navbar-brand'>leaderboards</a>

					</div>
				</div>

				<div class="navbar-right">
					@if (Auth::check())
					<span class="navbar-brand">Hi <a href="{{ url('/profile')}}"><strong>{{Auth::user()->username}}</strong></a></span>
					<a href="{{ url('/logout') }}" class='navbar-brand'>Logout</a>
					@else
					<a href="{{ url('/signup') }}" class='navbar-brand'>Signup</a> <span class="navbar-brand">|</span> <a href="{{ url('/login') }}" class='navbar-brand'>Login</a>
					@endif
				</div>

			</div><!--end .container-fluid -->
		</nav>


		@yield('content')
		<small>This site uses <a href="http://getbootstrap.com">Bootstrap</a> and <a href="http://glyphicons.com/">Glypicons</a> with pride</small>
	</div>

	<!-- For the javascript to work. Lesson: Include javascript at the end of the page -->
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>


</body>


</html>
