<?php

$adverbs = Adverb::all();

$new_collection = $adverbs->map(function($adverb)
{
  $adverb->definitions = Definition::where('adverb_id', '=', $adverb->id)->get();

  return $adverb;
});


echo "<ul>";
foreach($new_collection as $adverb){
  echo "<li>$adverb->adverb</li>";
  echo "<ul>";

  foreach ($adverb->definitions as $definition){

    echo "<li>$definition->body <strong>Sentence:</strong> $definition->example</li>";
  }
  echo "</ul>";
}
echo "</ul>";
