@extends('layout')

@section('content')

{{ Breadcrumbs::render('edit', $id) }}

<div class="panel panel-info">
  <div class="panel-heading ">Edit your definition</div>
</div>

{{ Form::open(array('url' => "definitions/$definition->id", 'method' => 'put')) }}

<div class="panel panel-default">

  <div class="panel-heading">Adverb {{ $errors->first('adverb', '<span class="label label-warning">:message</span>') }}</div>
  {{ Form::text('adverb', $definition->adverb()->adverb, array('class' => 'form-control', 'placeholder' => 'Adverb like "diabolically"')) }}
  <div class="panel-heading">Definition {{ $errors->first('body', '<span class="label label-warning">:message</span>') }}</div>
  {{ Form::textarea('body', $definition->body, array('class' => 'form-control', 'rows' => 5, 'placeholder' => 'Definition body')) }}
  <div class="panel-heading">Example sentence {{ $errors->first('example', '<span class="label label-warning">:message</span>') }}</div>
  {{ Form::text('example', $definition->example, array('class' => 'form-control', 'rows' => 5, 'placeholder' => 'Example sentence like "The Devil\'s not evil, he\'s just diabolically challenged."')) }}
</div>
{{ Form::button("<span class='glyphicon glyphicon-pencil'></span> Edit", array('class' => 'btn btn-primary btn-lg', 'type' => 'submit')) }}

{{ Form::close() }}

@stop
