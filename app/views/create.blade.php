@extends('layout')

@section('content')

{{ Breadcrumbs::render('create') }}

<div class="panel panel-default">
  <div class="panel-body">Got a witty "challenged" definition for an adverb? Go for it man!</div>
</div>

{{ Form::open(array('url' => 'definitions')) }}

<div class="panel panel-default">

  <div class="panel-heading">Adverb {{ $errors->first('adverb', '<span class="label label-warning">:message</span>') }}
    @if($errors->has('adverb') && $errors->first('adverb') == "That's not in our database, sorry")
    <span>Well, it SHOULD!</span> {{ Form::checkbox('create_adverb', 'yes')}}
    @endif
  </div>
  {{ Form::text('adverb', Input::old('adverb'), array('class' => 'form-control', 'placeholder' => 'Adverb like "diabolically"')) }}
  <div class="panel-heading">Definition {{ $errors->first('body', '<span class="label label-warning">:message</span>') }}</div>
  {{ Form::textarea('body', Input::old('body'), array('class' => 'form-control', 'rows' => 5, 'placeholder' => 'Definition body')) }}
  <div class="panel-heading">Example sentence {{ $errors->first('example', '<span class="label label-warning">:message</span>') }}</div>
  {{ Form::text('example', Input::old('example'), array('class' => 'form-control', 'rows' => 5, 'placeholder' => 'Example sentence like "The Devil\'s not evil, he\'s just diabolically challenged."')) }}
</div>
{{ Form::button("<span class='glyphicon glyphicon-pencil'></span> Create", array('class' => 'btn btn-primary btn-lg', 'type' => 'submit')) }}

{{ Form::close() }}

@stop
