@extends('layout')

@section('content')

{{ Breadcrumbs::render('browse') }}

@if(Session::get('message'))
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  {{ Session::get('message') }}
</div>
@endif

<p>Browse away my friend!</p>

<!-- navigation by letter -->
@if (isset($navLetter))
<ul class="pagination">
  @foreach($letters as $letter)
  <li {{ (($navLetter === $letter) ? " class='active'" : '' ) }}><a href='/definitions/{{ $letter }}'>{{ ucfirst($letter) }}</a></li>
  @endforeach
</ul>
@else
<ul class="pagination">
  @foreach($letters as $letter)
  <li {{ (($letter === "a") ? " class='active'" : '' ) }}><a href='/definitions/{{ $letter }}'>{{ ucfirst($letter) }}</a></li>
  @endforeach
</ul>
@endif

@if(!$adverbs->count())
<p>Oh, looks like there aren't any -ly adverbs for <kbd>{{ ucfirst($navLetter) }}</kbd>. Either that or I haven't updated the database. :P</p>
@endif

<div class="panel-group" id="accordion"> <!-- Accordian awesomeness! -->
@foreach($adverbs as $adverb)
  <div class="panel panel-default">   <!-- Start panel -->
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$adverb->adverb}}">
          {{$adverb->adverb}}
        </a>
      </h4>
    </div>
    <div id="collapse{{$adverb->adverb}}" class="panel-collapse collapse">
      @if(!$adverb->definitions->count())
      <div class="panel-body">    <!-- item -->
        <h3>Sorry, no definitions yet!</h3>
        <p>Make one? <a href="{{ url('definitions/create') }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil"></span>Create</a></p>
      </div>
      @endif
      @foreach($adverb->definitions as $definition)
      <div class="panel-body">    <!-- item -->
        <h3 {{ (isset(Auth::user()->id) && Auth::user()->id == $definition->user_id) ? "  class='text-info'": ''}}>{{ ucfirst($definition->adverb()->adverb) }} Challenged <small><strong>{{$definition->user()->username}}{{ (isset(Auth::user()->id) && Auth::user()->id == $definition->user_id) ? " <span class='glyphicon glyphicon-user'></span>": ''}}</strong></small></h3>
        <p>User Points: <span class="badge">{{ $definition->points() }}</span> | Guest Points: <span class="badge">{{$definition->guest_points}}</span></p>
        <p>{{ $definition->body }}</p>
        <p><strong>Example: </strong>{{ $definition->example }}</p>
        <p><a href='{{ url("definitions/$definition->id") }}' class="btn btn-default btn-sm"><span class="glyphicon glyphicon-chevron-right"></span>View</a></p>
      </div>                      <!-- end item -->
      @endforeach
    </div>
  </div>                              <!-- end panel -->
@endforeach

</div>


@stop
