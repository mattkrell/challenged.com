@extends('layout')

@section('content')

{{ Breadcrumbs::render('vote') }}

<h1>User Vote</h1>
<p class="bg-success">A users vote is a BIG vote! It counts for extra points to to the definitions total score! Yay!</p>


<div class="panel panel-default">
  <div class="panel-body">
    <h3>{{ $entry->adverb()->adverb }} challenged<small> by <strong>{{ $entry->user()->username }}</strong></small></h3>
    <p>User Points: <span class="badge">{{ $entry->points() }}</span> | Guest Points: <span class="badge">{{$entry->guest_points}}</span></p>
  </div>
</div>
<div class="well">
  <h4>Definition</h4>
  <p>{{ $entry->body }}</p>
  <h5>Example</h5>
  <p>{{ $entry->example }}</p>
</div>
{{ Form::open() }}
<p>
  {{ Form::button("<span class='glyphicon glyphicon-remove'></span>Lame", array('class' => 'btn btn-danger btn-lg', 'type' => 'submit', 'name' => 'like', 'value' => '0')) }}
  {{ Form::hidden('PointType', 'user') }}
  {{ Form::hidden('definition', $entry->id) }}

  @if ($entry->liked_user(Auth::user()->id))
  {{ Form::button("<span class='glyphicon glyphicon-ok'></span>Liked!", array('class' => 'btn btn-success btn-lg', 'name' => 'like', 'type' => 'submit', 'value' => '1')) }}
  @else
  {{ Form::button("<span class='glyphicon glyphicon-plus'></span>UserPoints", array('class' => 'btn btn-primary btn-lg', 'name' => 'like', 'type' => 'submit', 'value' => '1')) }}
  @endif
</p>
{{ Form::close() }}

@stop
