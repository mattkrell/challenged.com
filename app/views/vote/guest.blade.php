@extends('layout')

@section('content')

{{ Breadcrumbs::render('vote') }}

<h1>Guest Vote</h1>
<div class="bg-info">
<p class="bg-info">A guest vote is one standard vote. A vote by a <strong>user</strong>, however, is much more points!</p>

<p class="bg-info">Want to <strong>start making your own?</strong> <a href="{{ url('signup') }}" class="btn btn-primary btn-sm">Sign up</a> now! It's free and fast!</p>

<p class="bg-info">Have an account? <a href="{{ url('login') }}" class="btn btn-primary btn-sm">Log in</a> now!</p>
</div>


<div class="panel panel-default">
  <div class="panel-body">
    <h3>{{ $entry->adverb()->adverb }} challenged<small> by <strong>{{ $entry->user()->username }}</strong></small></h3>
    <p>User Points: <span class="badge">{{ $entry->points() }}</span> | Guest Points: <span class="badge">{{$entry->guest_points}}</span></p>
  </div>
</div>
<div class="well">
  <h4>Definition</h4>
  <p>{{ $entry->body }}</p>
  <h5>Example</h5>
  <p>{{ $entry->example }}</p>
</div>
{{ Form::open() }}
<p>
  {{ Form::button("<span class='glyphicon glyphicon-remove'></span>Lame", array('class' => 'btn btn-danger btn-lg', 'type' => 'submit', 'name' => 'like', 'value' => '0')) }}
  {{ Form::hidden('PointType', 'guest') }}
  {{ Form::hidden('definition', $entry->id) }}
  {{ Form::button("<span class='glyphicon glyphicon-plus'></span>GuestPoints", array('class' => 'btn btn-primary btn-lg', 'type' => 'submit', 'name' => 'like', 'value' => '1')) }}
{{ Form::close() }}
</p>


@stop

@stop
