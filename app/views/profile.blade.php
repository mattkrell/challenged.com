@extends('layout')

@section('content')

{{ Breadcrumbs::render('profile') }}

<!-- Nav tabs -->
<ul class="nav nav-tabs">
<li class="active"><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
  <li><a href="#definitions" data-toggle="tab"><span class="glyphicon glyphicon-file"></span> Definitions</a></li>
  <li><a href="#settings" data-toggle="tab"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">

  <div class="tab-pane active" id="profile"> <!-- PROFILE -->
    <div class="panel panel-info">
      <div class="panel-heading"><h2>{{Auth::user()->username}}</h2><h4>Rank: <span class="badge">{{Auth::user()->rank()}}</span> of <span class="badge">{{User::all()->count()}}</span></h4><a href="{{route('leaderboards')}}">Leaderboards</a></div>
      <div class="panel-body">
        <h3>Total Points: <span class="badge">{{Auth::user()->total_points()}}</span></h3>
        <h3>Guest Points: <span class="badge">{{Auth::user()->guest_points()}}</span></h3>
        <h3>UserPoints: <span class="badge">{{Auth::user()->user_points()}}</span></h3>
      </div>
    </div>
  </div>


  <div class="tab-pane" id="definitions"> <!-- DEFINITIONS -->
    <div class="panel-group" id="accordion"> <!-- Accordian awesomeness! -->
      @foreach($definitions as $definition)
        <div class="panel panel-default">   <!-- Start panel -->
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$definition->id}}">
                {{$definition->adverb()->adverb}}
              </a>
            </h4>
          </div>
          <div id="collapse{{$definition->id}}" class="panel-collapse collapse">
            <div class="panel-body">    <!-- item -->
              <h3 {{ (isset(Auth::user()->id) && Auth::user()->id == $definition->user_id) ? "  class='text-info'": ''}}>{{ ucfirst($definition->adverb()->adverb) }} Challenged <small><strong>{{$definition->user()->username}}{{ (isset(Auth::user()->id) && Auth::user()->id == $definition->user_id) ? " <span class='glyphicon glyphicon-user'></span>": ''}}</strong></small></h3>
              <p>User Points: <span class="badge">{{ $definition->points() }}</span> | Guest Points: <span class="badge">{{$definition->guest_points}}</span></p>
              <p>{{ $definition->body }}</p>
              <p><strong>Example: </strong>{{ $definition->example }}</p>
              <p><a href='{{ url("definitions/$definition->id") }}' class="btn btn-default btn-sm"><span class="glyphicon glyphicon-chevron-right"></span>View</a></p>
            </div>                      <!-- end item -->
          </div>
        </div>                              <!-- end panel -->
      @endforeach

      </div>
  </div>

  <div class="tab-pane" id="settings"> <!-- SETTINGS -->
    <div class="tab-pane active" id="profile">
      <div class="panel panel-info">
        <div class="panel-heading">Username</div>
        <div class="panel-body">
          <div class="form form-inline" role="form">
          {{Form::open(array('url' => 'profile/usernamereset'))}}
            <div class="form-group">
              {{ Form::text('username', Auth::user()->username, array('class' => 'form-control')) }}
            </div>
          {{ Form::button("Reset Username", array('class' => 'btn btn-primary btn-sm', 'type' => 'submit')) }}
          {{ Form::close() }}
          </div>
        </div>
        <div class="panel-heading">Email</div>
        <div class="panel-body">{{Auth::user()->email}}</div>
        <div class="panel-heading">Password</div>
        <div class="panel-body">
        <div class="form form-inline" role="form">
        <form>
          <div class="form-group">
            {{ Form::button("Reset Password", array('class' => 'btn btn-primary btn-sm', 'type' => 'submit', 'name' => 'email', 'value' => Auth::user()->email)) }}
          </div>
        </form>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
