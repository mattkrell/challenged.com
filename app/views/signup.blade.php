@extends('layout')

@section('content')


{{ Form::open() }}

  <div class="form-group">
    <h4>Email {{ $errors->first('email', '<span class="label label-warning">:message</span>') }}</h4>
    {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'email'))}}
  </div>
  <div class="form-group">
    <h4>Username {{ $errors->first('username', '<span class="label label-warning">:message</span>') }}</h4>
    {{ Form::text('username', Input::old('username'), array('class' => 'form-control', 'placeholder' => 'username'))}}
  </div>
  <div class="form-group">
    <h4>Password {{ $errors->first('password', '<span class="label label-warning">:message</span>') }}</h4>
    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'password')) }}
  </div>
  <div class="form-group">
    <h4>Confirm Password</h4>
    {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'password')) }}
  </div>
  <button type="submit" class="btn btn-default">Submit</button>

{{ Form::close() }}

@stop()
