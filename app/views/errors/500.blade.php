@extends('layout')

@section('content')

<div class="jumbotron">
  <h1>UH OH!</h1>
  <h2>500 INTERNAL SERVER ERROR</h2>
  <p>It looks as if the site is down for maintenance or something. If you have questions contact Matt at <code>mattckrell@gmail.com</code></p>
</div>

@stop
