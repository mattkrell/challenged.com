@extends('layout')

@section('content')

{{ Breadcrumbs::render('single', $id) }}

<div class="panel panel-info">
  <div class="panel-heading">
    <h3>{{ $entry->adverb()->adverb }}<small> by <strong>{{ $entry->user()->username }}</strong></small></h3>
    <p>User Points: <span class="badge">{{ $entry->points() }}</span> | Guest Points: <span class="badge">{{$entry->guest_points}}</span></p>
    <small>Created at: {{ $entry->created_at }} | Updated at: {{ $entry->updated_at }}</small>
  </div>
</div>
<div class="well">
  <h4>Definition</h4>
  <p>{{ $entry->body }}</p>
  <h5>Example</h5>
  <p>{{ $entry->example }}</p>
</div>

{{-- Edit if this is the user's --}}
@if( Auth::check() && $entry->user()->id == Auth::user()->id )
<p><a href='{{ url("definitions/$entry->id/edit") }}' class="btn btn-default btn-lg"><span class="glyphicon glyphicon-wrench"></span> Edit</a>
<a href='' class="btn btn-danger btn-lg" data-toggle="modal" data-target="#delete_modal"><span class="glyphicon glyphicon-remove"></span> Delete</a></p>
@endif

<!-- Modal -->
<div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="deleteModal">Delete Definition</h4>
      </div>
      <div class="modal-body">
        Are you SURE you want to delete this definition? It will be gone FOREVER!
      </div>
      <div class="modal-footer">
        {{ Form::open(array("url" => "definitions/$entry->id", "method" => "delete")) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        {{ Form::button("Delete", array('class' => 'btn btn-danger', 'type' => 'submit')) }}
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>

{{-- Show vote buttons, with GuestPoints and no Lame --}}
{{ Form::open(array('url' => 'vote')) }}
<p>
  {{ Form::button("<span class='glyphicon glyphicon-plus'></span>GuestPoints", array('class' => 'btn btn-default btn-lg', 'type' => 'submit', 'name' => 'PointType', 'value' => 'guest')) }}
  {{ Form::hidden('like', "1") }}
  {{ Form::hidden('definition', $entry->id) }}
  {{ Form::hidden('page', "definitions") }}

  @if (Auth::check() && $entry->liked_user(Auth::user()->id))
  {{ Form::button("<span class='glyphicon glyphicon-ok'></span>Liked!", array('class' => 'btn btn-success btn-lg', 'name' => 'PointType', 'type' => 'submit', 'value' => '1')) }}
  @elseif(Auth::check())
  {{ Form::button("<span class='glyphicon glyphicon-plus'></span>UserPoints", array('class' => 'btn btn-primary btn-lg', 'name' => 'PointType', 'type' => 'submit', 'value' => 'user')) }}
  @else
  @endif
</p>
{{ Form::close() }}



@stop
