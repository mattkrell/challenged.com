<?php

class PageController extends BaseController {

  public function showVote()
  {
    $entry = Definition::all()->random(1);

    if(Auth::check()){
      return View::make('vote.user', compact('entry'));
    }
    return View::make('vote.guest', compact('entry'));
  }

  public function handleVote()
  {
    //Check to see if person liked or disliked
    if(Input::get('like') == "1") {  # user likes definition

      if (Input::get('PointType') == 'user'){
          //retrieve user and definition
          $definition = Definition::find(Input::get('definition'));
          $definition_id = $definition->id;
          $user = Auth::user();
          $user_id = $user->id;

          //Check if relationship is already there
          if ($definition->liked_user($user_id)){  #relationship already exists

            # user likes this definition, so do nothing and redirect

          }else{
            //add many_to_many relationship between user and definition
            $definition->likes()->save($user);

            //redirect
          }

      }else{
          //+1 to guest points
          $definition = Definition::find(Input::get('definition'));

          $definition->guest_points += 1;
          $definition->save();

          //redirect
      }

      //check to see if another site page, like definitions/{id}, is using this code, and redirect accordingly.
      if(Input::has('page')){
        if(Input::get('page') == 'definitions'){
          return Redirect::to('/definitions')->with('message', '<span class="glyphicon glyphicon-ok"></span> Vote saved! Thank you');
        }
      }
      //redirect to /vote
      return Redirect::to('/vote');

    }else{  #user dislikes
      return Redirect::to('/vote');
    }
  }

}
