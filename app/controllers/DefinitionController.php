<?php

class DefinitionController extends \BaseController {

	public $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
		'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');

	public function __construct(){
		$this->beforeFilter('auth', array('only' =>
                            array('create')));
		$this->beforeFilter('csrf', array('only' =>
														array('store')));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//Show word + definition and edit buttons by pages, on page A
		$letters = $this->alphabet;

		$adverbs = Adverb::where('adverb', 'LIKE', 'a%')->get();

		$adverbs->map(function($adverb){   #put definitions in $defninition variable
			$adverb->definitions = Definition::where('adverb_id', '=', $adverb->id)->get();
			return $adverb;
		});

		return View::make('browse', compact('letters', 'adverbs'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//create a defninition
		return View::make('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();

		$rules = array(
			'adverb' => 'required|alpha_num|exists:adverbs',
			'body' => 'required',
			'example'		=> 'required'
		);

		$messages = array(
			'exists' => "That's not in our database, sorry"
		);

		//determine if a new adverb must be inserted, and change rules accordingly
		if (Input::get('create_adverb') == "yes"){
			$rules = array(
				'adverb' => 'required|alpha_num',
				'body' => 'required',
				'example'		=> 'required'
			);
		}

		$validator = Validator::make($data, $rules, $messages);

		if($validator->passes())
		{
			if (Input::get('create_adverb') == "yes"){
				$adverb = new Adverb;
				$adverb->adverb = Input::get('adverb');
				$adverb->save();
			}

			$adverb = Adverb::where('adverb', '=', Input::get('adverb'))->first();

			//save the definition
			$definition = new Definition;
			$definition->adverb_id = $adverb->id;
			$definition->body = Input::get('body');
			$definition->example = Input::get('example');
			$definition->user_id = Auth::user()->id;
			$definition->save();

			return Redirect::route('home')->with('message', '<span class="glyphicon glyphicon-floppy-saved"></span> Definition Saved');

		}else{
			Input::flash();
			return Redirect::to('definitions/create')->withErrors($validator);
		}




	}

	/**
	 * Display the specified resource OR show page by letter. That a good way to do it?
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if (ctype_alpha($id)){
			$navLetter = $id;
			$letters = $this->alphabet;

			$adverbs = Adverb::where('adverb', 'LIKE', $id.'%')->get();

			$adverbs->map(function($adverb){   #put definitions in $defninition variable
				$adverb->definitions = Definition::where('adverb_id', '=', $adverb->id)->get();
				return $adverb;
			});

			return View::make('browse', compact('letters', 'adverbs','navLetter'));
		}

		$entry = Definition::findOrFail($id);


		//show a specific definition
		return View::make('single', compact('entry', 'id'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$definition = Definition::find($id);

		return View::make('edit', compact('definition', 'id'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//update definition
		$definition = Definition::find($id);
		$adverb = Adverb::where('adverb', '=', Input::get('adverb'))->first();

		$definition->adverb_id = $adverb->id;
		$definition->body = Input::get('body');
		$definition->example = Input::get('example');
		$definition->save();

		return Redirect::to('definitions')->with('message', '<span class="glyphicon glyphicon-floppy-saved"></span> Definition Updated. Thank you!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		$definition = Definition::find($id);
		//detach all likes, (many_to_many relationships)
		$definition->likes()->detach();

		//delete definition
		$definition->delete(); #waaaaaahhh!!! :(

		return Redirect::to('definitions')->with('message', '<span class="glyphicon glyphicon-fire"></span> Definition Deleted. You monster.');
	}

}
